package com.javaclass;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Worker {

    private String workerName;
    private short hoursWorked;
    private float wage_perHour;

    public void read() throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("***Enter Worker Details***");
        System.out.println("Enter worker name: ");
        workerName = br.readLine();
        System.out.println("Enter hours worked per day: ");
        hoursWorked = Short.parseShort(br.readLine());
        System.out.println("Enter wage per hour: ");
        wage_perHour = Float.parseFloat(br.readLine());
    }
    public float calcwg(){
        float wage;
        wage = wage_perHour * hoursWorked;
        return wage;
    }
    public float extraWage(){
        float wage = calcwg();
        float extra_paid;
            if(wage < 200 )
                extra_paid = 150;
            else extra_paid = 50;
          return extra_paid;
    }
    public float totalWage(){
        float total_wage = calcwg() + extraWage();
        return total_wage;
    }

    public void display(){
        System.out.println("Name of the worker: "+workerName);
        System.out.println("No. of hours worked: "+hoursWorked);
        System.out.println("Wage per hour: "+wage_perHour);
        System.out.println("Wage of the worker:  "+calcwg());
        System.out.println("Extra Wage: "+extraWage());
        System.out.println("Total Wage: "+totalWage());

    }
    public static void main(String[] args) throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        byte menu;
        boolean choice = true;
        Worker work = new Worker();

        while(choice){
            System.out.println("1. Enter Worker Details: ");
            System.out.println("2. Display worker wage breakup");
            System.out.println("3. Exit");
            System.out.println("Enter Your Choice: ");
            menu = Byte.parseByte(br.readLine());

            switch (menu){
                case 1: work.read();
                    break;
                case 2: work.display();
                    break;
                case 3: choice = false;
            }//End of switch
        }//End of while
    }//End of main
}//End of class
