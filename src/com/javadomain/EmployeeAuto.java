package com.javadomain;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class EmployeeAuto {
    private static long sempID;

    //Static Block
    static {
        sempID = 1000;
    }

    private long empId;
    private String empName;
    private float basic_Pay;

    public EmployeeAuto() {
        sempID++;
        empId = sempID;
    }

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        byte menu;
        boolean choice = true;
        EmployeeAuto emp[] = new EmployeeAuto[3];

        while (choice) {
            System.out.println("1. Enter Employee Data");
            System.out.println("2. Display Employee Data");
            System.out.println("3. Exit");
            System.out.println("Enter Your Choice: ");
            menu = Byte.parseByte(br.readLine());

            switch (menu) {
                case 1:
                    for (byte i = 0; i < 3; i++) {
                        emp[i] = new EmployeeAuto();
                        emp[i].readData();
                    }
                    break;
                case 2:
                    for (byte i = 0; i < 3; i++) {
                        emp[i].displayData();
                    }
                    break;
                case 3:
                    choice = false;
            }
        }
    }

    public void readData() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter Employee Name: ");
        empName = br.readLine();
        System.out.println("Enter Employee Basic Pay: ");
        basic_Pay = Float.parseFloat(br.readLine());
    }

    public float[] calculateSalary() {
        float[] totalSal;
        totalSal = new float[3];
        totalSal[0] = (basic_Pay * 50) / 100;
        totalSal[1] = (basic_Pay * 10) / 100;
        totalSal[2] = basic_Pay + totalSal[0] + totalSal[1];
        return totalSal;
    }

    public void displayData() {
        float totalSalary[];
        totalSalary = new float[3];
        totalSalary = calculateSalary();

        System.out.println("Employee Salary Details\n**************");
        System.out.println("Employee Name: " + empName);
        System.out.println("Employee ID: " + empId);
        System.out.println("Basic Pay: " + basic_Pay);
        System.out.println("DA: " + totalSalary[0]);
        System.out.println("HRA: " + totalSalary[1]);
        System.out.println("Total Salary: " + totalSalary[2]);
    }
}
