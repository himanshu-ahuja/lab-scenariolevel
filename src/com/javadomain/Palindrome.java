package com.javadomain;

import java.util.Scanner;

public class Palindrome {

    public static boolean isPalindrome(String str){
        char letter;
        for(byte i = 0; i < (str.length()/2); i++){
            if(str.charAt(i) != str.charAt(str.length() - i - 1))
                return false;
        }
        return true;
    }

    public static void main(String[] args){
        String str;
        String answer = "";
        int count = 0;
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter a string: ");
        str = sc.nextLine();

        String strarr[] = str.split(" ");


        for(byte i=0; i < strarr.length; i++){
                if(isPalindrome(strarr[i])){
                    count++;
                    answer += strarr[i] + " ";
                }
        }
        System.out.println("Number of Palindromes are "+count+" which are "+answer+" in a given string "+str+" ");
    }
}
